# Examen - projet (solo)

## Introduction

Aujourd'hui, le forge logiciel est au coeur du fonctionnement des équipes de développeur et d'administrateur, elle permet de gagner un temps précieux dans le procéssus de développement et de mise en production. Combiné à des technologies telle que Docker pour packager les applications, la forge permets de créer des chaînes de déploiement totalement automatisées. 

## Consignes 

* Créer un projet git “dockerApp” sur [Gitlab](https://gitlab.com) en important le projet suivant : https://github.com/aws-samples/aws-demo-php-simple-app
* Donnez accès `maintener` à ce projet à l'utilisateur Git : `formateur_thomas.gerardin@supdevinci-edu.fr` (dans le menu de gauche partie members)
* Utiliser des runners personnalisés (à créer sur une VM)
* Créer une chaîne de continus déploiement pour ce projet qui va :
    * Faire des tests unitaires avec _“phpunit”_ 
    * Construire l’image Docker
    * La tag avec l’ID du commit (variables CI Gitlab: $CI_COMMIT_SHA)
    * Push l’image dans le registry Gitlab du projet (utilisez les variables protégée)
    * Déployer l’image sur une machine (il est possible d'utiliser votre VM local) à l'aide de *docker run* (uniquement sur master)
    * Faire un job optionnel qui va créer une issue sur votre projet avec pour titre "TestsCharge" Gitlab (utilisez les variables protégée)
    * Faire un minimum de 10 commits en respectant les bonnes pratiques


## Notes

* N'hésitez pas à utiliser ce qui a été fait en TPs, [les corrections sont disponibles ici](https://gitlab.com/thger_formation/supdevinci_gitlab_correctiontps)
* Ne vous y prennez pas au dernier moment, comme vous l'avez vu il peut arriver que Gitlab.com soit indisponible.
* La docuementation de Gitlab est très complète n'hésitez pas à la consulter
* Réalisez le projet étapes par étapes, n'hésitez pas à mettre de côté les points qui vous posent problème pour la fin.
* Il est nécessaire de changer l'image FROM dans l'image Docker pour la version 8 de CentOS.
* Pour l'étape avec les tests unitaires, des erreurs vous seront remontés, cela est normal. Il vous faudra passer les tests (mettre en commentaire ou faire un `|| true` à la fin de la commande pour passer à la suite). L'exercice ne vise pas à les corriger.
* Tous les bonnes pratiques seront valorisées ! La manière de commit, l'utilisation des fonctionnalités comme les variables protégées etc...

## Rendu

* Le projet se termine :
    * le 7/11/20 à 23h00 pour les DO B
    * le 13/11/20 à 23h00 pour les DO A
* Vos projets seront cloné par moi-même en local, il ne sera donc plus possible de procéder à des modifications après se délais.